const bodyParser = require('body-parser');
const express = require('express');

const app = express();
//creo un servidor http partiendo de la instancia app de express
const server = require('http').Server(app);
const io = require('socket.io')(server);

//importar el router
const Router = require('./app/router')
    //instancia de express
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); //para enviaar sin problemas datos de un formulario

// para usar los archivos de la carpreta public
app.use(express.static('public'));

//usar elrouter
app.use('', Router)
    //motor de vistas
app.set('view engine', 'pug')

//Eventos


require('./app/sockets')(io);

server.listen(3000, function() {
    console.log('servidor corriendo en el puerto 3000')

});